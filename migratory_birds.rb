def migratory_birds(array)
# type_array = [] | only required for testing purposes
c1, c2, c3, c4, c5 = 0, 0, 0, 0, 0
type1, type2, type3, type4, type5 = 1, 2, 3, 4, 5
  array.each do |item|
    #type_array << item | only required for testing purposes
    case item
    when type1 then c1 += 1
    when type2 then c2 += 1
    when type3 then c3 += 1
    when type4 then c4 += 1
    when type5 then c5 += 1
    end
  end
  #p c1, c2, c3, c4, c5, type_array
  if (c1 > c2) && (c1 > c3) && (c1 > c4) && (c1 > c5)
    puts type1
  elsif (c2 > c1) && (c2 > c3) && (c2 > c4) && (c2 > c5)
    puts type2
  elsif (c3 > c1) && (c3 > c2) && (c3 > c4) && (c3 > c5)
    puts type3
  elsif (c4 > c1) && (c4 > c2) && (c4 > c3) && (c4 > c5)
    puts type4
  elsif (c5 > c1) && (c5 > c2) && (c5 > c3) && (c5 > c4)
    puts type5
  else
    puts "Nothing to output"
  end
end

p ary = [].tap { |a| 100.times { a.push rand(10) } }
migratory_birds(ary)

#the line on top generates an array of 100 items with random numbers from 0-9

# def faster_dups(ary)
# found = {}
# dups = []
# ary.each do |el|
# dups.push el if found[el]
# found[el] = true
# end
# dups.uniq
# end
#
# p faster_dups([1, 2, 3, 4, 5, 4, 3, 2, 1, 3, 4])
# p faster_dups(ary)
