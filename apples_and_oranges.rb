#s: integer, starting point of Sam's house location.
#t: integer, ending location of Sam's house location.

# the range is from s to t

#a: integer, location of the Apple tree.
#b: integer, location of the Orange tree.
#apples: integer array, distances at which each apple falls from the tree.
#oranges: integer array, distances at which each orange falls from the tree.

s = 7
t = 11

a = 5
b = 15

apples = [-2, 2, 1]
oranges = [5, -6]

def countApplesOranges(s, t, a, b, apples, oranges)
totalOrangeCount = 0
totalAppleCount = 0
  apples.each do |apple|
    if a + apple >= s && a + apple <= t
      totalAppleCount += 1
    end
  end
  oranges.each do |orange|
    if b + orange >= s && b + orange <= t
      totalOrangeCount += 1
    end
  end
  p totalAppleCount
  p totalOrangeCount
end
countApplesOranges(s, t, a, b, apples, oranges)
