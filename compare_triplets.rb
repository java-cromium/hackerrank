a = [2, 5, 7, 6, 4, 9]
b = [4, 3, 8, 6, 5, 8]

def compare_triplets(a,b)
  count_a, count_b = 0, 0
  new_arr = []
  pairs = a.zip(b)
  # p pairs.count { |c,u| c==u } | Computes number of values that match at the same position
  pairs.each do |num|
      if num[0] > num[1]
        count_a += 1
      elsif num[0] < num[1]
        count_b += 1
      end
    end
    new_arr[0] = count_a
    new_arr[1] = count_b
    p new_arr
end

compare_triplets(a,b)