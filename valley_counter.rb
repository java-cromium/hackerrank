string = "UDDUDDUUDUDU"

def valley_counter(n=0, string)
  operations = {"u" => 1, "d" => -1}
  chars = string.split(//)
  path = 0
  valleys = 0
  is_valley = false

  chars.each do |char|
    operation = char.downcase
    path = operations[operation] ? path + operations[operation]: path
    valleys = path === 0 && is_valley ? valleys + 1 : valleys
    is_valley = path < 0
  end
  valleys
end

p valley_counter(string)
