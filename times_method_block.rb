
# 10.times { |multiple| p (multiple + 1) * 3 }

# 1.upto(100_000_000.0) do |numero_elefante|
#   p "#{numero_elefante} elefante se balanceaba sobre la tela de 1 araña
#     como veian que resistía fueron a llamar a otro elefante"
# end
#
# text = "o texto"
#
# text.length.times { |run| p "counting the runs, \# #{run + 1}" }

# def unless_test(num)
#   unless num % 2 == 0
#     puts "There is a remainder, I'm just not sure how much it is."
#   end
# end
#
# unless_test(20)
#
#
def fizzbuzz(num)
  i = 1
  while i <= num

    if i % 15 == 0
      puts "Fizzbuzz"
    elsif i % 5 == 0
      puts "Buzz"
    elsif i % 3 == 0
      puts "Fizz"
    end
    i += 1
  end
end
#
# fizzbuzz(45)

#
# def fizzbuzz(num)
#   i = 1
#   while i <= num
#     case
#     when ((i % 3 == 0) && (i % 5 == 0))
#       puts "Fizzbuzz"
#     when i % 5 == 0
#       puts "Buzz"
#     when i % 3 == 0
#       puts "Fizz"
#     end
#     i += 1
#   end
# end
#
# fizzbuzz(45)

def fuel_counter(mass)
  fuel = (mass.to_f.floor / 3) - 2
  fuel
end

# p fuel_counter(12)
# p fuel_counter(14)
# p fuel_counter(1969)
# p fuel_counter(100756)
# puts
# p fuel_counter(102751)

# [1,"a",Object.new,:hi].inject({}) do |hash, item|
#   puts "new iteration"
#   puts "the value of hash is #{hash}"
#   puts "the value of item is #{item}"
#   hash[item.to_s] = item
#   p item
#   puts "The new value of the variable 'hash' is: "
#   p hash
# end
#
# # puts #Adding blank-space/line-break
# # p hash
#
# numbers = [3, 5, 7]
# num = "tres mil"
#
# numbers.each { |num| puts num }
# puts num
# puts
#
# numbers.each do |num|
#   puts num
# end
# puts num
# puts
#
# for num in numbers do
#   puts num
# end
# puts num
# puts
#
# for num in (0..5) do
#   puts num
# end
# puts num

# for i in (1..3) do
#   puts "Outer loop: i = " + i.to_s
# for k in (1..4) do
#   puts "Inner loop: k = " + k.to_s
# end
# end
#
# puts
#
# (1..3).each do |h|
#   puts "Outer loop; h = " + h.to_s
# (1..4).each do |r|
#   puts "Inner loop; r = " + r.to_s
# end
# end
#
# puts i
# puts k
# puts h
# puts r

class BankAccount
  attr_reader :balance

  def initialize
    @balance = 0
  end

  def deposit(amount)
    @balance += amount
  end

  def withdraw(amount)
    @balance -= amount
  end
end

account = BankAccount.new
p account.balance
account.deposit(289)
p account.balance
