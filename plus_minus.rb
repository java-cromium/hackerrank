# def plusMinus(arr)
# plus, minus, zeros = 0.0, 0.0, 0.0
# plus_arr, minus_arr, zeros_arr = [], [], []
#     arr.each do |num|        
#         if num > 0
#             plus += 1
#         elsif num < 0
#             minus += 1
#         else 
#             zeros += 1
#         end
#     end
#     plus = (plus / arr.size)
#     plus = '%.6f' % (plus)
#     minus = (minus / arr.size)
#     minus = '%.6f' % (minus)
#     zeros = (zeros / arr.size)
#     zeros = '%.6f' % (zeros)
#     p plus.to_f.round(6), minus.to_f.round(6), zeros.to_f.round(6)
# end

def plusMinus(arr)
    plus, minus, zeros = 0.0, 0.0, 0.0
        arr.each do |num|        
            if num > 0
                plus += 1
            elsif num < 0
                minus += 1
            else 
                zeros += 1
            end
        end
        plus = (plus / arr.size).to_f.round(6)
        minus = (minus / arr.size).to_f.round(6)
        zeros = (zeros / arr.size).to_f.round(6)
        p plus, minus, zeros
    end

num_array = [1, 12, 0, -3, 3, -8]
plusMinus(num_array)
