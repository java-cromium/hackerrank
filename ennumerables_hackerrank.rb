def iterate_colors(colors)
  arr = []
  colors.each do |color|
      arr << color
  end
  p arr
end

colors = %w(verde blanco rojo azul)

iterate_colors(colors)

# # p "prueba"

def skip_animals(animals, skip)
    new_animals = []
    animals.each_with_index do |animal, index|
        new_animals << index.to_s + ":" + animal.to_s if index >= skip
    end
    new_animals
end

def skip_animals_2(animals, skip)
    results = []
    animals.drop(skip).each_with_index do |animal, i|
        results << "#{i+skip}:#{animal}"
    end
    p results
end

skip_animals(%w(goat tiger lion fly fish ant leopard bear cat dog), 3)
skip_animals_2(%w(goat tiger lion fly fish ant leopard bear cat dog), 3)
