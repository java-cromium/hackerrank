# frozen_string_literal: true

def angryProfessor(k, a)
  on_time = 0
  late = 0
  a.each do |time|
    if time > 0
      late += 1
    elsif time <= 0
      on_time += 1
    end
  end
  if on_time <= k
    p 'YES'
  else
    p 'NO'
  end
end

arr = [-1, -3, 4, 2]

angryProfessor(5, arr)
