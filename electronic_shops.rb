keyboards = [4, 5, 6, 7, 8, 9]
drives = [5, 6, 8, 9, 12]
b = 18

def get_money_spent(keyboards, drives, b)
  if keyboards.min + drives.min >= b
    return -1
  elsif keyboards.max + drives.max < b
    return keyboards.max + drives.max
  end
  totals = []
  keyboards.map do |keyboard|
    drives.map do |drive|
      totals.push(keyboard + drive)
      p totals
    end
  end
  totals.push(b).sort!
  result = totals.index(b-1)
  p totals[result]
end

p get_money_spent(keyboards, drives, b)