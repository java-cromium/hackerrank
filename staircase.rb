def staircase2(n)
  (1).step(n, 1) { |i| str = " "*(n-i)+("#"*i); puts str }
end

staircase2(9)