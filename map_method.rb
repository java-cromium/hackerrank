require 'rounding'
grades = [63, 58, 77, 69, 88, 91, 98]
# rounded = grades.map do |grade|
#   grade.ceil_to(5)
# end
# p rounded

def rounding(array)
  result = []
    array.each do |number|
      if number.between?(0, 100)
        number = 10
        result << number
      end
    end
  p result
end

rounding(grades)

# require 'rounding'
#
# # Round to the nearest five.
# p 122.45.round_to(5)
# # => 125
#
# # Round down to the previous 2.5.
# p 123.45.floor_to(5)
# # => 122.5
#
# # Round down to the previous 2.5.
# p 123.45.ceil_to(5)
# # => 122.5
#
# def round_up(number_array)
#   result = []
#   number_array.each do |num|
#       if num > 37 && num < 40
#         num.ceil_to(5)
#         result << num
#       elsif num > 42 && num < 45
#         num.ceil_to(5)
#         result << num
#       elsif num > 47 && num < 50
#         num.ceil_to(5)
#         result << num
#       elsif num > 52 && num < 55
#         num.ceil_to(5)
#         result << num
#       elsif num > 57 && num < 60
#         num.ceil_to(5)
#         result << num
#       elsif num > 62 && num < 65
#         num.ceil_to(5)
#         result << num
#       elsif num > 67 && num < 70
#         num.ceil_to(5)
#         result << num
#       elsif num > 72 && num < 75
#         num.ceil_to(5)
#         result << num
#       elsif num > 77 && num < 80
#         num.ceil_to(5)
#         result << num
#       elsif num > 82 && num < 85
#         num.ceil_to(5)
#         result << num
#       elsif num > 87 && num < 90
#         num.ceil_to(5)
#         result << num
#       elsif num > 92 && num < 95
#         num.ceil_to(5)
#         result << num
#       elsif num > 97 && num < 100
#         num.ceil_to(5)
#         result << num
#       end
#     end
#   p result
# end
#
# round_up(grades)
